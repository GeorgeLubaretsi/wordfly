from django.contrib.postgres.fields import JSONField
from django.db import models
from .base import BaseModel


class Wordlist(BaseModel):
    name = models.CharField(max_length=255, blank=False, unique=True)
    words = JSONField(default=list, help_text="A simple array of words")

    def __str__(self):
        return self.name
