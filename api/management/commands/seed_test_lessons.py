import random

from django.core.management.base import BaseCommand

from api.models import Lesson, User, Wordlist


class Command(BaseCommand):
    help = "Seed the database with Lesson objects, for testing"

    @staticmethod
    def flip_coin():
        return [True, False][random.randint(0, 1)]

    def handle(self, *args, **options):
        usr = User.objects.first()
        if usr is None:
            usr = User.objects.create_user('e@mail.co', 'pa$$word')

        for wordlist in Wordlist.objects.all():
            # Create five fake lessons for each Wordlist
            for i in range(5):
                self.stdout.write(f"Seeding lesson {i+1} of 5 for {wordlist.name}")

                # Instantiate our lesson object with an empty data container
                lesson = Lesson()
                lesson.data = dict()
                lesson.owner = usr

                # For each word, provide two data points
                # 1: How many times did the student attempt to identify this word?
                # 2: Did they ultimately identify the word correctly?
                for word in wordlist.words:
                    word_data = {
                        'attempts': random.randint(0,20),
                        'nailedIt': self.flip_coin()
                    }
                    lesson.data[word] = word_data

                lesson.save()


