from django.test import TestCase
from api.models import User, Lesson
from rest_framework.test import APIClient


class LessonViewsetTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(
            email='test@test.com', password='test')
        cls.staff_user = User.objects.create_user(
            email='staff@test.com', password='test', is_staff=True)

    def setUp(self):
        self.client = APIClient()

    def testUnauthCannotList(self):
        resp = self.client.get('/api/v1/lessons/')
        self.assertEqual(resp.status_code, 401)

    def testUnauthCannotRetrieve(self):
        with self.assertRaises(Lesson.DoesNotExist):
            Lesson.objects.get(id=1)
        resp = self.client.get('/api/v1/lessons/1/')
        self.assertEqual(resp.status_code, 401)  # not 404

    def testUnauthCannotCreate(self):
        payload = {
            'data': {'word1': 1, 'word2': 2}
        }
        resp = self.client.post(
            '/api/v1/lessons/', data=payload, format='json')
        self.assertEqual(resp.status_code, 401)

    def testUnauthCannotUpdate(self):
        lesson = Lesson.objects.create(owner=self.user, data={'test': 1})
        resp = self.client.put(
            '/api/v1/lessons/{}/'.format(lesson.id), data={'test': 2})
        self.assertEqual(resp.status_code, 401)

    def testUnauthCannotPartialUpdate(self):
        lesson = Lesson.objects.create(owner=self.user, data={'test': 1})
        resp = self.client.patch(
            '/api/v1/lessons/{}/'.format(lesson.id), data={'test': 2})
        self.assertEqual(resp.status_code, 401)

    def testCanOnlyReadOwnedLessons(self):
        # create lesson owned by staff user
        lesson = Lesson.objects.create(owner=self.staff_user, data={'test': 1})
        # set APIClient credentials to normal user
        self.client.credentials(
            HTTP_AUTHORIZATION='Token ' + self.user.spa_token)
        # client should get empty list
        resp = self.client.get('/api/v1/lessons/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json()['count'], 0)
        resp = self.client.get('/api/v1/lessons/{}/'.format(lesson.id))
        self.assertEqual(resp.status_code, 404)

    def testCanOnlyUpdateOwnedLessons(self):
        # create lesson owned by staff user
        lesson = Lesson.objects.create(owner=self.staff_user, data={'test': 1})
        # set APIClient credentials to normal user
        self.client.credentials(
            HTTP_AUTHORIZATION='Token ' + self.user.spa_token)
        resp = self.client.patch(
            '/api/v1/lessons/{}/'.format(lesson.id), data={'test': 2})
        self.assertEqual(resp.status_code, 404)
        resp = self.client.put(
            '/api/v1/lessons/{}/'.format(lesson.id), data={'test': 2})
        self.assertEqual(resp.status_code, 404)
