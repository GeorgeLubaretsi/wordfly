"""
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
"""
from django.urls import path, include
from rest_framework import routers
from .views import WordlistViewSet, LessonViewSet, UserViewSet

router = routers.SimpleRouter()
router.register(r'wordlists', WordlistViewSet, basename='wordlists')
router.register(r'lessons', LessonViewSet, basename='lessons')
router.register(r'users', UserViewSet, basename='users')

urlpatterns = [
    path('', include(router.urls)),
]
