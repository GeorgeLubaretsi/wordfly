from api.views.serializers.fields import WordlistDataField
from api.models import Wordlist
from rest_framework import serializers


class WordlistDataSerializer(serializers.Serializer):

    words = WordlistDataField()


class WordlistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wordlist
        fields = (
            'id', 'owner', 'created', 'modified', 'name', 'words'
        )

        read_only_fields = (
            'id', 'owner', 'created', 'modified', 'name', 'words'
        )
